﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathForGames2021
{
    class MathFormulas
    {
        static public float Quadratic(float x) // 𝑓(𝑥)=𝑥2+2𝑥–7
        {
            float y = x * x + 2 * x - 7;
            return y;
        }

        static public bool Quadratic(float a, float b, float c, out float root1, out float root2) //𝑓(𝑎,𝑏,𝑐)= −𝑏± √𝑏2−4𝑎𝑐  /2𝑎
        {
            float sqrtparam = b * b - 4 * a * c;
            if (sqrtparam >= 0 && (a != 0))
            {
                // Calculate the roots
                root1 = (-b + (float)Math.Sqrt(sqrtparam)) / 2 * a;
                root2 = (-b - (float)Math.Sqrt(sqrtparam)) / 2 * a;
                return true;
            }
            else
            {
                root1 = 0;
                root2 = 0;
                return false;
            }    
        }

        static public float LinearBlend(float s, float e, float t) //𝐿(𝑠,𝑒,𝑡)=𝑠+𝑡(𝑒−𝑠)
        {
            float var = s + t*(e - s);
            return var;
        }

        static public float DistanceBetweenPoints(float x1, float x2, float y1, float y2)  //𝐷(𝑃1,𝑃2)=√(𝑥2−𝑥1)2+(𝑦2−𝑦1)2
        {
            float D = (float)Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
            return D;
        }

        static public float ProductOfTwoPoints(float Px, float Py, float Pz, float Qx, float Qy, float Qz) //𝐼𝑛𝑛𝑒𝑟(𝑃,𝑄)=𝑃𝑥𝑄𝑥+𝑃𝑦𝑄𝑦+𝑃𝑧𝑄𝑧
        {
            float inner = Px * Qx + Py * Qy + Pz * Qz;
            return inner;
        }
                                                                                                                            //        (𝑎𝑥0+𝑏𝑦0+𝑐𝑧𝑜+𝑑)
        static public float DistnaceFromPlaneToPoint(float a, float b, float c, float d, float x0, float y0, float z0)        //𝐷(𝑃,𝑋0)=  √𝑎2+𝑏2+𝑐2
        {
            float distance = (a * x0 + b + y0 + c * z0 + d) / (float)Math.Sqrt(a * a + b * b + c * c);
            return distance;
        }
        static public float CubicBezierCurve(float t, float P0, float P1, float P2, float P3) //𝐵(𝑡,𝑃0,𝑃1,𝑃2,𝑃3)=(1−𝑡)3𝑃0+3(1−𝑡)2𝑡𝑃1+3(1−𝑡)𝑡2𝑃2+𝑡3𝑃3
        {
            float Curve = (1 - t) * 3 * P0 + 3 * (1 - t) * 2 * t * P1 + 3 * (1 - t) * t * 2 * P2 + t * 3 * P3;
            return Curve;
        }




        static void Main(string[] args)
        {

            float root1, root2;
            if (Quadratic(3, 4, 5, out root1, out root2))
            {
                Console.WriteLine($"Found roots to the equation. root1={root1}, root2={root2}");
            }
            else
            {

            }



        }
    }
}
