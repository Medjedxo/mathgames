﻿using System;

namespace Project2D
{
    public class Vector4
    {
        //Our floats to store the x, y, z, w, values in.
        public float x, y, z, w;

        //Basic Vector4 constructor.
        public Vector4()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 0;
        }

        //Overloaded Vector4 constructor, takes in 4 parameters.
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        //
        // NOTE:
        // These implicit operators will convert between a MathClasses.Vector4 & System.Numerics.Vector4
        // This is useful because Raylib has functions that work nicely with System.Numerics.Vector4, 
        // so we can pass our MathClasses.Vector4 directly to Raylib & let the implicit conversions do their job.         
        //public static implicit operator Vector4(System.Numerics.Vector4 v)
        //{
        //    return new Vector4 { x = v.X, y = v.Y, z = v.Z, w = v.W };
        //}

        //public static implicit operator System.Numerics.Vector4(Vector4 v)
        //{
        //    return new System.Numerics.Vector4(v.x, v.y, v.z, v.w);
        //}

        //Overloaded operator, used to add 2 vector4s and return a new vector4.
        public static Vector4 operator+(Vector4 left, Vector4 right)
        {
            return new Vector4(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
        }

        //Overloaded operator, used to subtract 2 vector4s and return a new vector4.
        public static Vector4 operator-(Vector4 left, Vector4 right)
        {
            return new Vector4(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
        }

        //Overloaded operator, used to prescale a new Vector4 by multiplying a left hand side(lhs) float and a Vector4.
        public static Vector4 operator*(float lhs, Vector4 rhs)
        {
            return new Vector4(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w);
        }

        //Overloaded operator, used to postscale a new Vector4 by multiplying a Vector4 by a right hand side(rhs) float.
        public static Vector4 operator*(Vector4 lhs, float rhs)
        {
            return new Vector4(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs);
        }

        //Function multiples all the component of Vector4 to get the value/magnitude of it.
        public float Magnitude()
        {
            return (float)Math.Sqrt(x * x + y * y + z * z + w * w);
        }

        //Function takes in Vector4 and uses below 'Normalize' to normolise it.
        public Vector4 Normalise(Vector4 val)
        {
            Vector4 result = new Vector4(val.x, val.y, val.z, val.w);
            result.Normalize();
            return result;
        }

        //Function first checks the Magnitude of the Vector4 and then divides each component by itself to get 1 in each axis, hence normolising it.
        public void Normalize()
        {
            float length = Magnitude();
            if (length > 0) 
            {
                x /= length;
                y /= length;
                z /= length;
                w /= length;
            }
        }

        //Fuction takes in a Vector4 and multiplyes each component by the initialized component. 
        public float Dot(Vector4 val)
        {
            return (this.x * val.x) + (this.y * val.y) + (this.z * val.z) + (this.w * val.w);
        }

        //This function takes a Vector4, multiplyes it by the default vector4 to result into a third vector4. 
        public Vector4 Cross(Vector4 val)
        {
            return new Vector4(y * val.z - z * val.y,
                               z * val.x - x * val.z,
                               x * val.y - y * val.x, 
                               0);
        }












    }
}
