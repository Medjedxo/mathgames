﻿using System;
using System.Windows;

namespace Project2D
{
    public class Vector3
    {
        //Our floats to store the x, y, z, values in.
        public float x, y, z;

        //Basic Vector3 constructor.
        public Vector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        //Overloaded Vector4 constructor, takes in 3 parameters.
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        //
        // NOTE:
        // These implicit operators will convert between a MathClasses.Vector3 & System.Numerics.Vector3
        // This is useful because Raylib has functions that work nicely with System.Numerics.Vector3, 
        // so we can pass our MathClasses.Vector3 directly to Raylib & let the implicit conversions do their job.         
        //public static implicit operator Vector3(System.Numerics.Vector3 v)
        //{
        //    return new Vector3 { x = v.X, y = v.Y, z = v.Z };
        //}

        //public static implicit operator System.Numerics.Vector3(Vector3 v)
        //{
        //    return new System.Numerics.Vector3(v.x, v.y, v.z);
        //}

        //Overloaded operator, used to add 2 vector3s and return a new vector3.
        public static Vector3 operator+(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
        }

        //Overloaded operator, used to subtract 2 vector3s and return a new vector3.
        public static Vector3 operator-(Vector3 lhs, Vector3 rhs)
        {
            return Sub(lhs, rhs);
        }

        //Overloaded operator, used to prescale a new Vector3 by multiplying a left hand side(lhs) float and a Vector3.
        public static Vector3 operator*(float lhs, Vector3 rhs)
        {
            return new Vector3(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
        }

        //Overloaded operator, used to postscale a new Vector4 by multiplying a Vector4 by a right hand side(rhs) float.
        public static Vector3 operator*(Vector3 lhs, float rhs)
        {
            return new Vector3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
        }

        //Function takes 2 Vector3s and subtract them to return a new vector3.
        public static Vector3 Sub(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
        }

        //Function multiples all the component of Vector4 to get the value/magnitude of it.
        public float Magnitude()
        {
            return (float)Math.Sqrt(x * x + y * y + z * z);
        }

        //Function first checks the Magnitude of the Vector4 and then divides each component by itself to get 1 in each axis, hence normolising it.
        public void Normalize()
        {
            float length = Magnitude();
            if (length > 0)
            {
                x /= length;
                y /= length;
                z /= length;
            }
        }

        //Fuction takes in a Vector3 and multiplyes each component by the initialized component.
        public float Dot(Vector3 val)
        {
            return (this.x * val.x) + (this.y * val.y) + (this.z * val.z);
        }

        //This function takes a Vector3, multiplyes it by the default vector3 to result into a third vector3.
        public Vector3 Cross(Vector3 val)
        {
            return new Vector3(y * val.z - z * val.y,
                               z * val.x - x * val.z,
                               x * val.y - y * val.x);
        }
    }
}
