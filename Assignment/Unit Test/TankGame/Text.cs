﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Raylib.Raylib;
using static Raylib.Font;

namespace Project2D
{
    class Text : SceneObject
    {
        private Raylib.Font textFont;
        private string stringData = "";
        private Raylib.Color textColor = Raylib.Color.RED;
        private int fontSize = 20;

        public string StringData
        {
            get => stringData;
            set => stringData = value;
        }

        public int FontSize
        {
            get => fontSize;
            set => fontSize = value;
        }

        public Raylib.Color TextColor
        {
            get => textColor;
            set => textColor = value;
        }

        public Text(string fontFileName)
        {
            textFont = LoadFont(fontFileName);
        }

        public Text()
        {
            textFont = GetFontDefault();
        }

        ~Text()
        {
            UnloadFont(textFont);
        }
        public virtual void OnDraw()
        {
            DrawText(stringData.ToString(), (int)globalTransform.m7, (int)globalTransform.m8, fontSize, textColor);
        }
    }
}
