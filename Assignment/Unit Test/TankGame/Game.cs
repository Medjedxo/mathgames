﻿using Raylib;
using System;
using System.Diagnostics;
using static Raylib.Raylib;
using MathClasses;
using System.Collections.Generic;

namespace Project2D
{
    class Game
    {
        Stopwatch stopwatch = new Stopwatch();

        private long currentTime = 0;
        private long lastTime = 0;
        private float timer = 0;
        private int fps = 1;
        private int frames;

        private float deltaTime = 0.005f;


        Image logo;
        Texture2D texture;

        List<SceneObject> sceneObjects = new List<SceneObject>();

        public Game()
        {
        }

        public void Init()
        {
            stopwatch.Start();
            lastTime = stopwatch.ElapsedMilliseconds;

            if (Stopwatch.IsHighResolution)
            {
                Console.WriteLine("Stopwatch high-resolution frequency: {0} ticks per second", Stopwatch.Frequency);
            }

            Text startup = new Text();
            startup.StringData = "TEST";
            startup.TextColor = Raylib.Color.DARKBLUE;
            startup.FontSize = 50;
            startup.Translate(100, 100);
            sceneObjects.Add(startup);

            //logo = LoadImage("..\\Images\\aie-logo-dark.jpg");
            //logo = LoadImage(@"..\Images\aie-logo-dark.jpg");
            //logo = LoadImage("../Images/aie-logo-dark.jpg");
            //texture = LoadTextureFromImage(logo);
        }

        public void Shutdown()
        {
        }

        public void Update()
        {
            lastTime = currentTime;
            currentTime = stopwatch.ElapsedMilliseconds;
            deltaTime = (currentTime - lastTime) / 1000.0f;
            timer += deltaTime;
            if (timer >= 1)
            {
                fps = frames;
                frames = 0;
                timer -= 1;
            }
            frames++;

            // insert game logic here            
        }

       

        public void Draw()
        {
            BeginDrawing();

            ClearBackground(Color.GREEN);

            DrawText(fps.ToString(), 10, 10, 14, Color.RED);

            DrawTexture(texture,
                GetScreenWidth() / 2 - texture.width / 2, GetScreenHeight() / 2 - texture.height / 2, Color.WHITE);

            EndDrawing();
        }

    }
}
