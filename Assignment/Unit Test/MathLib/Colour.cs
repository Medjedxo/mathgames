﻿using System;

namespace MathClasses
{
    public class Colour
    {
        public UInt32 colour;

        public Colour()
        {
            colour = 0;
        }

        public Colour (byte red, byte green, byte blue, byte alpha)
        {
            SetRed(red);
            SetGreen(green);
            SetBlue(blue);
            SetAlpha(alpha);
        }

        public byte GetRed ()
        {
            return (byte)((colour & 0xff000000) >> 24);
        }
        public byte GetGreen()
        {
            return (byte)((colour & 0x00ff0000) >> 16);
        }
        public byte GetBlue()
        {
            return (byte)((colour & 0x0000ff00) >> 8);
        }
        public byte GetAlpha()
        {
            return (byte)((colour & 0x000000ff) >> 00);
        }

        public void SetRed(byte value)
        {
            colour = colour & 0x00ffffff;
            colour |= ((UInt32)value << 24);
        }
        public void SetGreen(byte value)
        {
            colour = colour & 0xff00ffff;
            colour |= ((UInt32)value << 16);
        }
        public void SetBlue(byte value)
        {
            colour = colour & 0xffff00ff;
            colour |= ((UInt32)value << 8);
        }
        public void SetAlpha(byte value)
        {
            colour = colour & 0xffffff00;
            colour |= ((UInt32)value);
        }

    }
}
